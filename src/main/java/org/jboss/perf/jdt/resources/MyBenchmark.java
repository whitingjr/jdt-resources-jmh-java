/*
 * JBoss, Home of Professional Open Source.
 * Copyright 2014 Red Hat, Inc., and individual contributors
 * as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package org.jboss.perf.jdt.resources;

import java.io.File;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.internal.localstore.SafeChunkyOutputStream;
import org.eclipse.core.internal.localstore.SafeChunkyOutputStreamProposed;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.Blackhole;

/**
 * @author Jeremy Whiting
 */
@BenchmarkMode(Mode.Throughput)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
@Fork(value = 1)
public class MyBenchmark {

	@Benchmark public void original(Blackhole bh)
        throws Exception
    {
		File file = new File ( temp, String.format("%1$s.txt", System.currentTimeMillis()));
		try ( SafeChunkyOutputStream stream = new SafeChunkyOutputStream ( file ) ){
			byte[] bytes = new byte[2048];
			ran.nextBytes( bytes );
			stream.write ( bytes );
			stream.succeed();
			bh.consume( bytes );
		} finally {
			file.delete();
		}
    }

	@Benchmark public void proposed(Blackhole bh)
        throws Exception
    {
		File file = new File ( temp, String.format("%1$s.txt", System.currentTimeMillis()));
		try ( SafeChunkyOutputStreamProposed stream = new SafeChunkyOutputStreamProposed ( file ) ){
			byte[] bytes = new byte[2048];
			ran.nextBytes( bytes );
			stream.write ( bytes );
			stream.succeed();
			bh.consume( bytes );
		} finally {
			file.delete();
		}
    }

	@Setup (Level.Trial) public void setupOnceBefore()
        throws Exception
    {
        temp = new File( System.getProperty( "java.io.tmpdir" ), "bench");
        temp.mkdirs();
        
    }
	@TearDown (Level.Trial) public void tearDownOnceAfter()
    {
        temp.delete();
    }

	File temp;
	Random ran = new Random();
}
